# Lexunit ToDo

This project is a demo ToDo list app.

The project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

[Material-UI](https://github.com/mui-org/material-ui) is used to build the core UI and [Redux](https://github.com/reduxjs/redux) is used for internal state management.

## Installation

In the project root:
```
npm install 
```
or
```
yarn install
```

## Running in development mode

After installation in the project root:
```
npm run start
```
or
```
yarn start
```

## Running in production mode

After installation in the project root:
```
npm run prod
```
or
```
yarn prod
```

## Running tests

After installation in the project root:
```
npm run test
```
or
```
yarn test
```