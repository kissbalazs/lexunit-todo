// Importing used ES6 feature polyfills 
import 'core-js/fn/object/values';
import 'core-js/fn/array/includes';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
