const categories = {
  GOOD: 'good',
  BAD: 'bad',
  JUICY: 'juicy'
}
export default categories;

export const categoryValues = Object.values(categories);