import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';

const styles = {
  card: {
    width: 360,
    margin: 16,
  },
};

class LexunitCard extends PureComponent {
  render() {
    const {classes, children} = this.props;
    return (
      <Card className={classes.card}>
        {children}
      </Card>
    );
  }
}

LexunitCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LexunitCard);