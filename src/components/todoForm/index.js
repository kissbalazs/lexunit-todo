import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import { connect } from 'react-redux'
import { addTodo } from '../../actions'

import LexunitCard from '../lexunitCard';
import ToDoCategorySelector from '../todoCategorySelector';
import { categoryValues } from '../../constants/categories';

const styles = theme => ({
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 328 - 2 * theme.spacing.unit,
  },
  addButton: {
    marginLeft: 'auto'
  },
});

class ToDoForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      category: null
    }
  }

  handleNameChange = event => {
    this.setState({
      name: event.target.value,
    });
  };

  handleCategoryChange = category => this.setState({ category });

  validateInput = (name, category) => 
    new Promise((resolve, reject) => {
      let errorMsgs = [];
      if (name === '') {
        errorMsgs.push('add name')
      }
      if(!categoryValues.includes(category)){
        errorMsgs.push('select a category')
      }
      if(errorMsgs.length) {
        reject(`Please ${errorMsgs.join(' and ')} for the ToDo.`);
      }
      resolve();
    })

  handleAddTodo = () => {
    const { name, category } = this.state;
    this.validateInput(name, category)
      .then(() => this.props.dispatch(addTodo(name, category)))
      .catch(alert);
  }

  render() {
    const { classes } = this.props;
    const { name, category } = this.state;

    return (
      <LexunitCard>
        <CardContent>
          <Typography className={classes.title} color="textSecondary">
            Add ToDo
          </Typography>

          <TextField
            id="name"
            label="ToDo name"
            className={classes.textField}
            value={name}
            onChange={this.handleNameChange}
            margin="normal"
          />
          <ToDoCategorySelector onSelect={this.handleCategoryChange} category={category} />
        </CardContent>
        <CardActions>
          <Button className={classes.addButton} size="small" onClick={this.handleAddTodo}>Send</Button>
        </CardActions>
      </LexunitCard>
    );
  }
}

ToDoForm.propTypes = {
  classes: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect()(withStyles(styles)(ToDoForm));