import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { categoryValues } from '../../constants/categories';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  }
});

class ToDoCategorySelector extends PureComponent {
  handleCategoryClick = category => () => {
    this.props.onSelect(category)
  }

  getCategoryColor = category => {
    if (category === this.props.category) {
      return 'primary'
    }
    return 'default'
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        {categoryValues.map(category =>
          <Button variant='raised'
                  key={category}
                  className={classes.button}
                  onClick={this.handleCategoryClick(category)}
                  color={this.getCategoryColor(category)}>
            {category}
          </Button>
        )}
      </div>
    );
  }
}

ToDoCategorySelector.propTypes = {
  classes: PropTypes.object.isRequired,
  category: PropTypes.string,
  onSelect: PropTypes.func.isRequired,
};

export default withStyles(styles)(ToDoCategorySelector);