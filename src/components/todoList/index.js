import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import LexunitCard from '../lexunitCard';


const defaultHeaderStyle = {
  color: 'white',
  fontWeight: 'bold',
  textTransform: 'uppercase',
}

class ToDoList extends PureComponent {
  render() {
    const { category, elements, headerColor } = this.props;

    const headerStyle = {
      ...defaultHeaderStyle,
      backgroundColor: headerColor,
    }

    return (
      <LexunitCard>
        <List
          component="nav"
          subheader={<ListSubheader component="div" style={headerStyle}>{category}</ListSubheader>}
        >
          {elements.map((todo, index) =>
            <ListItem key={index}>
              <ListItemText primary={todo} />
            </ListItem>
          )}
        </List>
      </LexunitCard>
    );
  }
}

ToDoList.propTypes = {
  elements: PropTypes.array.isRequired,
  category: PropTypes.string.isRequired,
  headerColor: PropTypes.string.isRequired
};

export default ToDoList;