import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import ToDoForm from '../todoForm';
import ToDoListWrapper from '../todoListWrapper';

const styles = {
  root: {
    flexGrow: 1,
  },
  appBody: {
    marginTop: 15,
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-around'
  },
};

class ToDoApp extends PureComponent {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="title" color="inherit">
              Lexunit ToDo
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.appBody}>
          <ToDoForm />
          <ToDoListWrapper />
        </div>
      </div>
    );
  }
}

ToDoApp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ToDoApp);
