import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { categoryValues } from '../../constants/categories';
import ToDoList from '../todoList';

const styles = {
  root: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    alignContent: 'center',
    flexWrap: 'wrap',
    justifyContent: 'space-around'
  }
}

const listColors = [
  'green',
  'red',
  'gold'
]

class ToDoListWrapper extends PureComponent {
  render() {
    const { todos } = this.props;

    return (
      <div style={styles.root}>
        {categoryValues.map((category, index) => {
          const headerColor = listColors[index % listColors.length];
          return <ToDoList category={category} elements={todos[category]} key={category} headerColor={headerColor} />;
        })}
      </div>
    );
  }
}

ToDoListWrapper.propTypes = {
  todos: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  todos: state.todos
})

export default connect(mapStateToProps)(ToDoListWrapper);