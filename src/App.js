import React, { PureComponent } from 'react';

import { createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import persistState from 'redux-localstorage'

import ToDoApp from './components/todoApp';

const enhancer = compose(
  persistState(),
)

const store = createStore(rootReducer, {}, enhancer)

class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <ToDoApp />
      </Provider>
    );
  }
}

export default App;
