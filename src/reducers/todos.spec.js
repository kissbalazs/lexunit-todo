import todos from './todos';

describe('todos reducer', () => {
  it('should have initial state', () => {
    expect(
      todos(undefined, {})
    ).toEqual(
      {
        'good': [],
        'bad': [],
        'juicy': [],
      }
    );
  });

  it('should handle ADD_TODO', () => {
    expect(
      todos({
        'good': [],
        'bad': [],
        'juicy': [],
      }, {
          type: 'ADD_TODO',
          name: 'Some task',
          category: 'bad'
        })
    ).toEqual({
      'good': [],
      'bad': ['Some task'],
      'juicy': [],
    });

    expect(
      todos({
        'good': [],
        'bad': ['Some task'],
        'juicy': [],
      }, {
          type: 'ADD_TODO',
          name: 'Some task2',
          category: 'bad'
        })
    ).toEqual({
      'good': [],
      'bad': ['Some task', 'Some task2'],
      'juicy': [],
    });

    expect(
      todos({
        'good': [],
        'bad': ['Some task'],
        'juicy': [],
      }, {
          type: 'ADD_TODO',
          name: 'Some task2',
          category: 'juicy'
        })
    ).toEqual({
      'good': [],
      'bad': ['Some task'],
      'juicy': ['Some task2'],
    });

    expect(
      todos({
        'good': [],
        'bad': ['Some task'],
        'juicy': [],
      }, {
          type: 'ADD_TODO',
          name: 'Some task2',
          category: 'something else'
        })
    ).toEqual({
      'good': [],
      'bad': ['Some task'],
      'juicy': [],
    });
  });
});