import categories, { categoryValues } from '../constants/categories';

const initialTodosState = {
  [categories.GOOD]: [],
  [categories.BAD]: [],
  [categories.JUICY]: []
}

const addTodo = (state, {category, name}) => {
  if(!categoryValues.includes(category)) return state;
      
  return {
    ...state,
    [category]: [
      ...state[category],
      name
    ]
  }
}

const todos = (state = initialTodosState, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return addTodo(state, action);
    default:
      return state
  }
}

export default todos;