export const addTodo = (name, category) => ({
  type: 'ADD_TODO',
  name,
  category
});