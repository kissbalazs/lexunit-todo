import * as actions from './index';

describe('todo actions', () => {
  it('addTodo should create ADD_TODO action', () => {
    expect(actions.addTodo('To do something', 'juicy')).toEqual({
      type: 'ADD_TODO',
      name: 'To do something',
      category: 'juicy'
    });
  });
});